import {
	GET_TECHS,
	ADD_TECH,
	DELETE_TECH,
	SET_LOADING,
	TECHS_ERROR,
} from './types';

//Get techs from server
export const getTechs = () => async (dispatch) => {
	try {
		//Set loading to true
		setLoading();
		//Fetch data from /techs
		const res = await fetch('/techs');
		const data = await res.json();
		//Dispatch data to reducer
		dispatch({
			type: GET_TECHS,
			payload: data,
		});
	} catch (err) {
		//Dispatch error to reducer
		dispatch({
			type: TECHS_ERROR,
			payload: err.response.statusText,
		});
	}
};

//Add tech
export const addTech = (tech) => async (dispatch) => {
	try {
		//Set loading to true
		setLoading();

		const res = await fetch('/techs', {
			method: 'POST',
			body: JSON.stringify(tech),
			headers: {
				'Content-Type': 'application/json',
			},
		});
		const data = await res.json();
		//Dispatch data to reducer
		dispatch({
			type: ADD_TECH,
			payload: data,
		});
	} catch (err) {
		//Dispatch error to reducer
		dispatch({
			type: TECHS_ERROR,
			payload: err.response.statusText,
		});
	}
};

//Delete tech
export const deleteTech = (id) => async (dispatch) => {
	try {
		//Set loading to true
		setLoading();
		//Fetch data from /techs with a { method, body and header }
		await fetch(`/techs/${id}`, {
			method: 'DELETE',
		});
		//Dispatch data to reducer
		dispatch({
			type: DELETE_TECH,
			payload: id,
		});
	} catch (err) {
		//Dispatch error to reducer
		dispatch({
			type: TECHS_ERROR,
			payload: err.response.statusText,
		});
	}
};

//Set loading to true
export const setLoading = () => {
	return {
		type: SET_LOADING,
	};
};
