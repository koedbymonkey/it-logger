import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Preloader from '../layout/Preloader';
import LogItem from './LogItem';
import PropTypes from 'prop-types';
import { getLogs } from '../../actions/logActions';

//Destructures logs and loading from logReducer and getLogs from logActions
const Logs = ({ log: { logs, loading }, getLogs }) => {
	//useEffect is called on page load and calls getLogs
	useEffect(() => {
		getLogs();
		// eslint-disable-next-line
	}, []);

	//Shows Preloader if the page is still loading
	if (loading || logs === null) {
		return <Preloader />;
	}

	//Returns logs ul for each log that was fetched
	return (
		<ul className='collection with-header'>
			<li className='collection-header'>
				<h4 className='center'>System Logs</h4>
			</li>
			{!loading && logs.length === 0 ? (
				<p className='center'>No logs to show...</p>
			) : (
				logs.map((log) => <LogItem log={log} key={log.id} />)
			)}
		</ul>
	);
};

Logs.propTypes = {
	logs: PropTypes.object.isRequired,
	getLogs: PropTypes.func.isRequired,
};

//Gets log state from logReducer
const mapStateToProps = (state) => ({
	log: state.log,
});

export default connect(mapStateToProps, { getLogs })(Logs);
